package top.fomeiherz;

/**
 * 测试类
 *
 * @author fomeiherz
 * @date 2020/2/18 11:26
 */
public class StringCompilerTest {

    public static void main(String[] args) throws Exception {
        // 传入String类型的代码
        String source = "import java.util.Arrays;public class Main" +
                "{" +
                "public static void main(String[] args) {" +
                "System.out.println(Arrays.toString(args));" +
                "}" +
                "}";
        StringCompiler.run(source, "1", "2");
    }
    
}
